module.exports = {
  browserSync: {
    hostname: "https://jacquicodesample.ddev.site",
    port: 8080,
    openAutomatically: false,
    reloadDelay: 50
  },

  drush: {
    enabled: true,
    alias: 'drush --uri=jacquicodesample.ddev.site cache-rebuild all'
  },

  twig: {
    useCache: false // "false" means Drupal caches will be cleared on template changes.
  }
};

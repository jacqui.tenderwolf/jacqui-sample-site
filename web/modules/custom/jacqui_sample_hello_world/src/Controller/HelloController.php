<?php

namespace Drupal\jacqui_sample_hello_world\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * Defines HelloController class.
 */
class HelloController extends ControllerBase {

  /**
   * Display the markup.
   *
   * @return array
   *   Return markup array.
   */
  public function content($name) {
    return [
      '#type' => 'markup',
      '#markup' => $this->t(
        
        // Hello, they call me "Bob".
        'Hello, they call me "' . $name . '".'
      
      ),
    ];
  }

}
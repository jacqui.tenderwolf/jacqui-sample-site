<?php

namespace Drupal\jacqui_sample_html5_audio\Plugin\Field\FieldFormatter;

use Drupal\Component\Utility\Html;
use Drupal\Core\Field\FieldItemInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of the 'html5audio_field_formatter' formatter.
 *
 * @FieldFormatter(
 *   id = "html5audio_field_formatter",
 *   label = @Translation("HTML5 Audio"),
 *   field_types = {
 *     "link"
 *   }
 * )
 */
class Html5Audio extends FormatterBase {

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      'autoplay' => 0,
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    return [
        'autoplay' => [
          '#type' => 'radios',
          '#title' => $this->t('Autoplay'),
          '#default_value' => 1,
          '#options' => [
            0 => $this->t('Off'),
            1 => $this->t('On'),
         ]
      ] 
    ] + parent::settingsForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = [];
    $settings = $this->getSettings();
    if ($settings['autoplay'] == 1) {
      $summary['message'] = [
        '#type' => 'markup',
        '#markup' => $this->t('Autoplay is enabled'),
      ];
    }
    
    else {
      $summary['message'] = [
        '#type' => 'markup',
        '#markup' => $this->t('Autoplay is not enabled.'),
      ];
    }
    
  return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {

    $settings = $this->getSettings();
    $elements = [];

    // Render all field values as part of a single <audio> tag.
    $sources = [];
    foreach ($items as $delta => $item) {
      // Get the mime type.
      $mimetype = \Drupal::service('file.mime_type.guesser')->guess($item-> uri);
      $sources[] = [
        'src' => $item->uri,
        'mimetype' => $mimetype,
      ];
   }

   // Put everything in an array for theming.
      if ($settings['autoplay'] == 0) {
        $autoplay = '';
      }
      else { 
        $autoplay = 'autoplay'; 
      }
      

   $elements[] = [
      '#theme' => 'audio_tag',
      '#sources' => $sources,
      '#autoplay' => $autoplay,
    ];

   return $elements;
  }

  /**
   * Generate the output appropriate for one field item.
   *
   * @param \Drupal\Core\Field\FieldItemInterface $item
   *   One field item.
   *
   * @return string
   *   The textual output generated.
   */
  protected function viewValue(FieldItemInterface $item) {
    // The text value has no text format assigned to it, so the user input
    // should equal the output, including newlines.
    return nl2br(Html::escape($item->value));
  }

}
